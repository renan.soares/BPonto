from django.conf.urls import patterns, url
from horarios import views
from horarios.views import *

urlpatterns = patterns('',
        url(r'^login/', views.logina, name='logina'),
        url(r'^registrar/', views.register, name='registrar'),
        url(r'^sobre/', Sobre.as_view(), name='sobre'),
        url(r'^logout/', views.user_logout, name='logoutt'),
        url(r'^criate/', Criar.as_view(), name='criar'),
        url(r'^$', Listar.as_view(), name='listar1'),
        url(r'^home/', Listar.as_view(), name='listar'),
        #url(r'view/(?P<pk>\d+)/$', Detalhe.as_view(), name='detalhe'),
        url(r'update/(?P<pk>\d+)/$', Update.as_view(), name='update'),
        url(r'editar/(?P<pk>\d+)/$', UpdateUser.as_view(), name='updateuser'),
    	#url(r'delete/(?P<pk>\d+)$', Deletar.as_view(), name='author_delete'),
    	url (r'^usuarios/',ListarUsuarios.as_view(), name='usuarios'),
    	url(r'^users/(?P<pk>\d+)/$', AbrirUsuarios.as_view(), name='listarusuarios'),
    	url(r'^horarios/(?P<pk>\d+)/$', listaHorario.as_view(), name='listahorario'),


)