from django import forms
from horarios.models import Horario
#from inicio.models import Post
from django.contrib.auth.models import User
import time
import datetime

class UserForm(forms.ModelForm):
	username = forms.CharField(help_text="Login*")
	password = forms.CharField(widget=forms.PasswordInput(),help_text="Senha*")
	first_name = forms.CharField(help_text="Nome*")
	last_name = forms.CharField(help_text="Sobrenome*")
	email = forms.EmailField(help_text="Email*")
	class Meta:
		model = User
		fields = ['username', 'password', 'first_name', 'last_name', 'email']


class ContadorForm(forms.ModelForm):
    #inicio = datetime.datetime.now()
    inicio = forms.DateTimeField(required=False)
    fim = forms.DateTimeField(required=False)
    horas = forms.DecimalField(required=False)
    ativo = forms.BooleanField(required=False)

    def __init__(self, *args, **kwargs):
        #print "Ola aaa"
        super(ContadorForm, self).__init__(*args, **kwargs)
    class Meta:
        model = Horario
        fields = ("inicio", "fim", "horas", "ativo")

