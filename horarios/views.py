# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from horarios.forms import UserForm, ContadorForm
from horarios.models import Horario
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import FormView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse_lazy
from django.utils import timezone
from datetime import timedelta, datetime
from django.db.models import Sum
from django.shortcuts import redirect

def logina(request):
    context = RequestContext(request)
    if request.method == 'POST':
        usuario = request.POST['username']
        senha = request.POST['password']
        user = authenticate(username=usuario, password=senha)
        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/home/' + usuario)
            else:
                return HttpResponse("Sua conta esta desabilitada!")
        else:
            print "Invalid login details: {0}, {1}".format(user, senha)

            return redirect('listar')
    else:
        return render_to_response('login.html', {}, context)


def register(request):
    context = RequestContext(request)
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)

        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            registered = True

        else:
            print user_form.errors,
    else:
        user_form = UserForm()


    return render_to_response('register.html', {'user_form': user_form, 'registered': registered}, context)


class Sobre(TemplateView):

    template_name = "sobre.html"

    def dispatch(self, *args, **kwargs):
        return super(Sobre, self).dispatch(*args, **kwargs)


@login_required
def user_logout(request):
    logout(request)
    # Take the user back to the homepage.
    return HttpResponseRedirect('/login')


class Criar(CreateView):
    model = Horario
    fields = ['inicio', 'ativo']
    template_name = 'criar.html'
    form_class = ContadorForm
    success_url = "../../home"

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(Criar, self).dispatch(*args, **kwargs)

    def get_form(self, form_class):
        form = super(Criar, self).get_form(form_class)
        return form

    def form_valid(self, form):

        h = timezone.now();

        if not Horario.objects.filter(ativo=1).last():
            form_teste = form.save(commit=False)
            form_teste.user = self.request.user
            form_teste.inicio = h
            form_teste.ativo = True
            # form_teste.fim = h + timedelta(seconds=27000) #por enquanto
            # form_teste.horas = ((form_teste.fim - form_teste.inicio).seconds)/3600 #por enquanto
            form_teste.save()

            return super(Criar, self).form_valid(form)


class Listar(ListView):

    model = Horario
    template_name = 'listar.html'


    def get_queryset(self, **kwargs):
        return Horario.objects.filter(user = self.request.user)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(Listar, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(Listar, self).get_context_data(**kwargs)
        context['ativo'] = Horario.objects.filter(ativo=1).last()
        return context

class Update(UpdateView):
    model = Horario
    fields = ['inicio', 'fim', 'horas', 'ativo']
    template_name = 'criar.html'
    form_class = ContadorForm
    success_url = '../../home'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(Update, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(Update, self).get_context_data(**kwargs)
        context['ativo'] = Horario.objects.filter(ativo=1).last()
        return context

    def get_form(self, form_class):
        form = super(Update, self).get_form(form_class)
        return form

    def form_valid(self, form):
        #print form
        h = timezone.now()

        atual = Horario.objects.filter(ativo=1).last()
        if atual:
            form_teste = form.save(commit=False)
            form_teste.user = self.request.user
            #form_teste.fim = h

            form_teste.inicio = atual.inicio
            form_teste.fim = h # + timedelta(seconds=27000) #por enquanto

            form_teste.horas = ((form_teste.fim - atual.inicio).seconds)/3600.0 #por enquanto
            form_teste.ativo = False
            form_teste.save()
            #print self.object
            return super(Update, self).form_valid(form)


class UpdateUser(UpdateView):
    model = User
    fields = ['first_name', 'last_name', 'email', 'password']
    template_name  = 'editaruser.html'
    success_url = '../../home'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UpdateUser, self).dispatch(*args, **kwargs)

    def form_valid(self, form):

        form_teste = form.save(commit=False)
        form_teste.set_password(form_teste.password)
        form_teste.save()
        return super(UpdateUser, self).form_valid(form)

class ListarUsuarios(ListView):
    model = User
    template_name = 'usuarios.html'
    context_object_name = 'listadeusuario'

    def get_queryset(self, **kwargs):
        return User.objects.all()

class AbrirUsuarios(ListView):
    model = Horario
    template_name = 'usuarios_teste.html'
    ano_atual = timezone.now().year
    mes_atual = timezone.now().month
    context_object_name = 'coisas'
    #x = Post()
    #x.user = user = User.objects.get(id=1)
    #print x.user
    def get_queryset(self, **kwargs):
        pk=self.kwargs['pk']
        #print pk
        return Horario.objects.filter(user = User.objects.get(id=pk))

    def get_context_data(self, **kwargs):
        pk = self.kwargs['pk']
        context = super(AbrirUsuarios, self).get_context_data(**kwargs)
        context['ativo'] = Horario.objects.filter(user = User.objects.get(id=pk))
        context['none_mes'] = False
        context['none_ano'] = False
        mes_ext = {1: 'Janeiro', 2: 'Fevereiro', 3: 'Março', 4: 'Abril', 5: 'Maio', 6: 'Junho', 7: 'Julho', 8: 'Agosto',
                   9: 'Setembro', 10: 'Outubro', 11: 'Novembro', 12: 'Dezembro'}

        mes = str(Horario.objects.filter(user = User.objects.get(id=pk), inicio__month=self.mes_atual).aggregate(Sum('horas')).values()[0])
        if mes == 'None':
            context['mes'] = 'Não possui nenhum ponto registrado'
            context['none_mes'] = True
        else:
            tempoM = mes.split('.')
            context['mes'] = '%s horas e %s minutos' % (tempoM[0], tempoM[1])
            context['mesAtual'] = mes_ext[self.mes_atual]

        ano = str(Horario.objects.filter(user = User.objects.get(id=pk), inicio__year=self.ano_atual).aggregate(Sum('horas')).values()[0])
        if ano == 'None':
            context['ano'] = 'Não possui nenhum ponto registrado'
            context['none_ano'] = True
        else:
            tempoA = ano.split('.')
            context['ano'] = '%s horas e %s minutos' % (tempoA[0], tempoA[1])
            context['anoAtual'] = self.ano_atual

            #context['teste'] = Horario.objects.filter(user = User.objects.get(id=pk))

        return context

class listaHorario(ListView):
    model = Horario
    template_name = 'lista_horarios.html'
    context_object_name = 'horarios'
    ano_atual = timezone.now().year
    mes_atual = timezone.now().month


    def get_query(self, **kwargs):
        pk=self.kwargs['pk']
        return Horario.objects.filter(user_id=pk)

    def get_context_data(self, **kwargs):
        pk = self.kwargs['pk']
        context = super(listaHorario, self).get_context_data(**kwargs)
        context['ativo'] = Horario.objects.filter(user_id=pk)

        mes_ext = {1: 'Janeiro', 2: 'Fevereiro', 3: 'Março', 4: 'Abril', 5: 'Maio', 6: 'Junho', 7: 'Julho', 8: 'Agosto',
                   9: 'Setembro', 10: 'Outubro', 11: 'Novembro', 12: 'Dezembro'}

        mes = str(Horario.objects.filter(inicio__month=self.mes_atual).aggregate(Sum('horas')).values()[0])
        tempoM = mes.split('.')
        context['mes'] = '%s horas e %s minutos' % (tempoM[0], tempoM[1])
        context['mesAtual'] = mes_ext[self.mes_atual]

        ano = str(Horario.objects.filter(inicio__year=self.ano_atual).aggregate(Sum('horas')).values()[0])
        tempoA = ano.split('.')
        context['ano'] = '%s horas e %s minutos' % (tempoA[0], tempoA[1])
        context['anoAtual'] = self.ano_atual

        return context