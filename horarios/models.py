from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.core.urlresolvers import reverse



class Horario(models.Model):

    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    inicio = models.DateTimeField(blank=True, null=True)
    fim = models.DateTimeField(blank=True, null=True)
    horas = models.DecimalField(decimal_places=2, max_digits=8, null=True, blank=True)
    ativo = models.BooleanField(blank=True)

    def get_absolute_url(self):
        return reverse("/basico/home/", kwargs={"id": self.id})

    def __unicode__(self):
        return self.id